import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WinnerService {

  constructor() { }
  winnerId: number = null

   column = (grille:Array<any>) => {
    

    for(let r:number = 0; r < 3; r++){
      if(grille[0][r] == 1 && grille[1][r] == 1 && grille[2][r] == 1){
        this.winnerId = 1
      }

      if(grille[0][r] == 0 && grille[1][r] == 0 && grille[2][r] == 0){
        this.winnerId = 0
      }
    }
  }

  row = (grille:Array<any>) =>{
   
    for(let c:number = 0; c < 3; c++){
      if(grille[c][0] == 1 && grille[c][1] == 1 && grille[c][2] == 1){
        this.winnerId = 1
      }

      if(grille[c][0] == 0 && grille[c][1] == 0 && grille[c][2] == 0){
        this.winnerId = 0
      }   
    }    
  }


  

  diagonal = (grille:Array<any>) => {
 
      if(grille[0][0] == grille[1][1] && grille[1][1] == grille[2][2]){
        this.winnerId = grille[0][0]
      }


      if(grille[0][2] == grille[1][1] && grille[1][1] == grille[2][0]){
        this.winnerId = grille[0][2]
      }
  }

    returnWinner = (grille:Array<any>) => {
      this.row(grille)
      this.column(grille)
      this.diagonal(grille)

      return this.winnerId
    }


    resetWinner = () =>{
      this.winnerId = null
    }
}
