import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomNumberService {

  constructor() { }

  randomNumber = (min, max) => {
    return Math.floor(Math.random()*(max-min) + min)
  }
}
