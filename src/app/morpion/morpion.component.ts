import { Component, Input, Output,EventEmitter,OnInit,ElementRef  } from '@angular/core';
import { Joueur } from '../Models/joueur'
import { RandomNumberService } from '../services/random/random-number.service'
import { WinnerService } from '../services/winner/winner.service'


@Component({
  selector: 'app-morpion',
  templateUrl: './morpion.component.html',
  styleUrls: ['./morpion.component.scss']
})

export class MorpionComponent implements OnInit {

  //Reçoit les informations des 2 joueurs
  @Input() players: Joueur[];
   
  currentPlayer: number
  currentNamePlayer : string
  winnerPlayer: string = null
  compteur:number = 0
  draw = false

  //Initialise une grille vide
  grille = [
    [null,null,null],
    [null,null,null],
    [null,null,null]
  ]

  gameNotfinish = true

  //Appel de nos services
  constructor(
      private RandomNumberService:RandomNumberService,
      private WinnerService:WinnerService,
      private el:ElementRef
    ){}

  //Lors de l'appel du composant
  //Choisi un nombre entre 0 et 1 aléatoirement
  //Et stock le nom du joueur qui commence dans une variable, qui sera affichée sur la page HTML
  ngOnInit() {

    this.currentPlayer = this.RandomNumberService.randomNumber(0,2)
    this.currentNamePlayer = this.players[this.currentPlayer].Nom

  }


  //Cette méthode est appelée lors du clique sur une cellule
  //Elle prend 3 paramètres, les coordonnées (ligne et colonne) et l'input cliqué
  play = (row:number,column:number,input) => {

      //Vérifie si la cellule cliqué est bien null (via les coordonnées) et qu'il n'y a pas de gagnant
      if(this.grille[column][row] == null && this.winnerPlayer == null){

        //Donc si la cellule est null, null est remplacé par l'id du joueur
        this.grille[column][row] =  this.currentPlayer

        //Ajoute la classe "cirlce" ou "cross" suivant quel joueur à cliqué
        input.target.classList.add(this.players[this.currentPlayer].Type)
  
        //Permet de vérifier toutes les combinaisons, si il trouve une combinaison gagnante alors retourne l'id gagnant
        let winnerNumber:number = this.WinnerService.returnWinner(this.grille)     
          
        //Dans le cas ou il n'y a toujours pas de gagnant le jeu continue
        if(winnerNumber === null){
  
          //Condition ternaire qui permet de switcher de joueur 
          this.currentPlayer == 0 ? this.currentPlayer = 1 : this.currentPlayer = 0
          //Affiche le nom du joueur qui doit jouer sur la page
          this.currentNamePlayer = this.players[this.currentPlayer].Nom
          //Incrémente un compteur en cas d'égalité
          this.compteur++
          
          //En cas d'égalité, si compteur est = à 9 (9 cellules) et qu'il n'y a pas de gagant, alors
          if(this.compteur == 9 && winnerNumber === null){
            //Affiche la div "egalité" et propose au joueur de rejouer
            this.draw = true
          }

          //Dans le cas ou il y a un gagnat
        }else{
          //Affichage de la div "gagnat"
          this.gameNotfinish = false
          //Affecte le nom du gagant à la variable qui sera ensuite afficher sur la page
          this.winnerPlayer = this.players[winnerNumber].Nom    
        }
      }  
  }


  //Méthode pour réinitialiser le jeu
  restart = () => {

    //Vide la grille de jeu
    this.grille = [
      [null,null,null],
      [null,null,null],
      [null,null,null]
    ]

    //Supprime toutes les classes CSS ajoutées (pour l'affichage des cercles et croix)
    this.el.nativeElement.querySelectorAll('#cell').forEach(cell => {
      cell.classList.remove('circle')
      cell.classList.remove('cross')
    })

    //Remet les différentes varibales à l'état initiale
    this.gameNotfinish = false
    this.winnerPlayer = null
    this.compteur = 0
    this.draw = false

    //Régénére un joueur aléatoirement
    this.currentPlayer = this.RandomNumberService.randomNumber(0,2)
    this.currentNamePlayer = this.players[this.currentPlayer].Nom

    //Réinitialise le joueur gagnant (du service WINNER)
    this.WinnerService.resetWinner()
  }
}
