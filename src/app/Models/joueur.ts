export class Joueur{
    
    //Attributs
    Nom : string
    Id : number
    Type : string

    constructor(Id:number, Nom:string, Type:string){
       this.Nom = Nom;
       this.Id = Id;
       this.Type = Type; 
    }

    //Méthodes
    getId = () => {
        return this.Id;
    }

    getNom = () => {
        return this.Nom;
    }

    getColor = () => {
        return this.Type;
    }

}