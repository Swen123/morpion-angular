import { Component } from '@angular/core'
import { Joueur } from './Models/joueur'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Le jeu du morpion'
  
  showMain = true
  showMorpion = false

  players: Joueur[]

//Apres avoir saisie les noms 
//Cette méthode sera appelée lors de l'évènement "SendRequestToFather"
//Affecte l'objet reçu à la variable players
  showPlayers = (input: Joueur) => {
    this.players = [
      input[0],
      input[1]
    ];
  
    //cache le formulaire et affiche le jeu
    this.showMain = false
    this.showMorpion = true
  }
}
