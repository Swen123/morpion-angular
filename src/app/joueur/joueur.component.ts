import { Component, Output, EventEmitter } from '@angular/core'
import { Joueur } from '../Models/joueur'

@Component({
  selector: 'app-joueur',
  templateUrl: './joueur.component.html',
  styleUrls: ['./joueur.component.scss']
})

export class JoueurComponent {

  @Output() SendRequestToFather = new EventEmitter<Joueur[]>();

  //Variable contenant les Noms des joueurs elles sont donc initialisées à vide, pour commencer
  playerOne = '' 
  playerTwo = ''

  //Variables de type Joueur (Objet joueurs)
  player_One: Joueur
  player_Two: Joueur

  //Messaage d'erreur vide car aucun message
  error = ''
  //Affichage de la div d'erreur (pas afficher au début)
  showError = false

  //Cette méthode permet dans un premier temps de vérifier si les champs joueurs sont bien saisies 
  //et que les noms des joueurs soient bien différents.
sendPlayers = () => {

  //Vérifie si les champs ne sont pas vide
  if(this.playerOne == '' || this.playerTwo == ''){

    // si les champs sont vide alors affiche un message d'erreur
     this.error = 'Les champs sont vides, veuillez les remplir pour pouvoir commencer.' 
     this.showError = true

     //Sinon si les nom sont pareil
  }else if(this.playerOne == this.playerTwo || this.playerTwo == this.playerOne){

    //Affiche un message d'erreur
    this.error = 'Les noms des joueurs doivent être différent.' 
    this.showError = true

    //Dans le cas ou tout est bon
  }else{
    
    //Créé (instancie) deux objets joueur 1 et joueur 2, passage en paramètre de l'id, le nom du joueur saisie et le type 
    //Croix ou cercle
    this.player_One = new Joueur(1, this.playerOne, 'cross');
    this.player_Two = new Joueur(2,this.playerTwo,'circle')
    

    //Et enfin envoi les données (les 2 objets) au parent
    this.SendRequestToFather.emit([
      this.player_One,
      this.player_Two
    ]);    
  }

}

}
