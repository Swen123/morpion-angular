import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { MorpionComponent } from './morpion/morpion.component';
import { JoueurComponent } from './joueur/joueur.component';

@NgModule({
  declarations: [
    AppComponent,
    MorpionComponent,
    JoueurComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
